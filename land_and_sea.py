# find if a coord is in water or on land

import numpy as np

from matplotlib import pyplot as plt

map_data = np.fromfile("gl-latlong-1km-landcover.bsq", dtype=np.uint8)

map_data = map_data.reshape(21600, 43200)


def interpret_coords(coord_string):
    """unpack coord sting in format '48.95˚N 9.13˚E'"""
    coords_strings = coord_string.split()
    coords = (get_angle(coords_strings[1]), get_angle(coords_strings[0]))
    validate_coords(coords)
    return coords


def validate_coords(coords):
    x, y = coords
    if x >= 180.0 or x <= -180.0:
        raise ValueError("invalid W/E coord")
    if y >= 90.0 or y <= -90.0:
        raise ValueError("invalid N/S coord")


def get_angle(coord_string):
    degrees = float(coord_string[:-2])
    sign = coord_string[-1]
    if sign == "S" or sign == "W":
        degrees *= -1
    elif sign == "N" or sign == "E":
        pass
    else:
        raise ValueError("invalid position string")
    return degrees


def read_textfile(file_name):
    with open(file_name, "r") as f:
        coords = [readline(line) for line in f if line[0] != "#"]
    return coords


def readline(line):
    line = line.split(" ")[1]
    lat = line.split(";")[1]
    lon = line.split(";")[2]
    validate_coords((float(lat), float(lon)))
    return (float(lat), float(lon))


def plot_box(coords):
    five = int(5.0 / 0.0083333333)
    pixel_i, pixel_j = get_pixels(coords)
    data_box = map_data[
        pixel_i - five : pixel_i + five, pixel_j - five : pixel_j + five
    ]
    print(data_box.shape)
    plt.imshow(data_box)
    plt.show()


def get_pixels(coords):
    coord_x, coord_y = coords[0] + 180.0, -coords[1] + 90
    pixel_j = int(coord_x / 0.0083333333) % 43200
    pixel_i = int(coord_y / 0.0083333333) % 21600
    return pixel_i, pixel_j


def get_terrain_type(coords):
    pixel_i, pixel_j = get_pixels(coords)
    return map_data[pixel_i, pixel_j]

#This is the plotting branch
plot_box(interpret_coords("0˚N 12˚E"))